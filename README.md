# Testeur d'équivalence pour PCF probabiliste

Ce repertory contient les différentes productions réalisées lors de mon stage de L3, dans l'équipe LSC du laboratoire LIS à Marseille.

Plus spécifiquement, il s'agit de mon rapport de stage sous forme de source latex (et le pdf associé), ainsi que le code OCaml qui illustre mon sujet.

Le code se présente sous deux versions (deux implémentations distinctes), nommées "classique" et "rigide", qui sont décrites dans le rapport. Pour les deux implémentations, les fichiers de code se trouvent dans le répertoire lib. 

