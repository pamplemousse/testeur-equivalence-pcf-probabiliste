type t

val create : int -> int -> t
val add : t -> t -> t
val sub : t -> t -> t
val mult : t -> t -> t
val div : t -> t -> t

val zero : t
val one : t
val one_half : t

val print : t -> unit