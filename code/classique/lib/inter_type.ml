type inter_type =
  | Base of bool
  | Map of (inter_type_mset * inter_type)

and inter_type_mset = (inter_type * int) list (*invariant : l'entier est >0 *)

let card (a : inter_type_mset) : int = List.fold_left (fun sum (_,n) -> sum + n) 0 a

(* false < true *)
let bool_compare (a : bool) (b : bool) : int =
  match (a, b) with
  | (true, false) -> 1
  | (false, true) -> -1
  | _ -> 0

(* hypothèse : les entrées de compare et ms_compare sont sous forme canonique *)
let rec compare (a : inter_type) (b : inter_type) : int =
  match (a, b) with
  | (Base b1, Base b2) -> bool_compare b1 b2
  | (Base _, _) -> -1
  | (Map (ma, sa), Map (mb, sb)) -> let cmp = compare sa sb in
      if (cmp != 0) then cmp else (ms_compare ma mb) (* évite des appels récursifs/comparaisons inutiles*)
  | (Map _, Base _) -> 1

(* ordre lexicographique *)
and ms_compare (a : inter_type_mset) (b : inter_type_mset) : int = 
  let na = card a in let nb = card b in
  if (na <> nb) then (na - nb)
  else 
    let rec aux (a : inter_type_mset) (b : inter_type_mset) : int =
      match (a, b) with
      | ([], []) -> 0
      | (_, []) -> 1
      | ([], _) -> -1
      | ((t1, n1)::aa, (t2,n2)::bb) -> let cmp = compare t1 t2 in
      if (cmp <> 0) then cmp else (* évite des appels récursifs/comparaisons inutiles*)
      if (n1 <> n2) then (n2 - n1) else (aux aa bb)
    in aux a b
and canonical (a : inter_type) : inter_type =
  match a with
  | Base _ -> a
  | Map (m, b) -> Map (ms_canonical m, canonical b)

and ms_canonical (m : inter_type_mset) : inter_type_mset = 
  let step1 = (List.map (fun (a,n) -> (canonical a, n)) m) in
  let step2 = List.sort (fun (a,_) (b,_) -> compare b a) step1 in (* order is reversed*)
  fuse step2 (* reversed again*)

and fuse (m : inter_type_mset) : inter_type_mset =
  let rec aux (m : inter_type_mset) (last : (inter_type * int) option) (res : inter_type_mset) : inter_type_mset =
    match (m, last) with
    | ([], None) -> res
    | ([], Some(v,i)) -> (v,i)::res
    | ((e,n)::mm, None) -> aux mm (Some (e,n)) res
    | ((e,n)::mm, Some(v,i)) ->
        if (compare e v = 0) then (aux mm (Some (e, n+i)) res)
        else (aux mm (Some (e,n)) ((v,i)::res))
  in aux m None []

let create = canonical
let create_ms = ms_canonical

let add_elem (a: inter_type) (n : int) (m : inter_type_mset) : inter_type_mset =
  (*hyp : a et m sont sous forme canonique*)
  let rec aux (m : inter_type_mset) : inter_type_mset =
    match m with
    | [] -> [(a,n)]
    | (b,k)::mm -> 
      let cmp = compare a b in
      if (cmp = 0) then ((b,k+n)::mm) else (*a = b*)
      if (cmp < 0) then ((a,n)::(b,k)::mm) (*a < b*)
      else (b,k)::(aux mm) (*a > b*)
  in aux m

(* ! we assume below that those elements are canonicals *)
let eqtype (a:inter_type) (b:inter_type) = (a=b)
let eqtypemset (ma:inter_type_mset) mb = (ma = mb)

(* lazy version ; TODO : some sort of merge/fusion *)
let add_mset a b = ms_canonical (List.rev_append a b)

let rec print_it (a : inter_type) =
  match a with
  | Base b -> if b then print_string "tt" else print_string "ff" 
  | Map (m, s) -> print_mset m ; print_string " ⊸ " ; print_it s
and print_mset (m : inter_type_mset) = print_char '[' ; (List.iter (fun (a,n) -> 
  if (n = 1) then (print_it a ; print_string " ; ")
  else (print_string "(" ; print_it a; print_string ")^" ; print_int n ; print_string " ; "))
   m); (if (m <> []) then print_string "\b\b\b") ; print_char ']'


open Termes
(* type ty = Termes.ty (* | Base | Func of (ty * ty) *)
type variable = char *)

let rec print_t (a : ty) = match a with Base -> print_char 'B'
  | Func (b, c) -> print_char '(' ; print_t b ; print_string "→" ; print_t c ; print_char ')'
(* 
type t_star = | Star | Base | Func of (t_star * t_star) (* Star représente n'importe quel type*)
let rec t_star_of_t (a : ty) : t_star =
  match a with
  | Base -> Base
  | Func (c, d) -> Func(t_star_of_t c, t_star_of_t d)

(* TODO : fix, return generalized type*)
let rec unifie (a : t_star) (b : t_star) : bool =
  match (a,b) with
  | (Star, _) -> true
  | (_, Star) -> true
  | (Base, Base) -> true
  | (Func (c1, d1), Func(c2, d2)) -> (unifie c1 d1) && (unifie c2 d2)
  | _ -> false


(* TODO : fix, cf unifie*)
let rec raffine_star (a : inter_type) : t_star =
  match a with
  | Base _ -> Base
  | Map (l, r) ->
      let f = fun acc (e,_) -> begin
        let a = raffine_star e in
        match acc with
        | None -> Some a
        | Some b -> if (unifie a b) then acc else (raise (Failure "le multiensemble de départ comprend des types distincts"))
      end in begin
      match (List.fold_left f None l) with
        | None -> Func (Star, raffine_star r)
        | Some v -> Func (v, raffine_star r)
      end

let raffinable (a : inter_type) : bool = try (let _ = (raffine_star a) in true) with _ -> false

let raffine (a : inter_type) (t : ty) : bool = unifie (t_star_of_t t) (raffine_star a)
 *)
type inter_contexte = (variable * inter_type_mset * ty) list 

type r = Rationals.t

let print_ic (g : inter_contexte) = List.iter (fun (v,m,t)-> 
  print_char v ; print_string " : " ; print_mset m ; print_string " ◁ " ; print_t t ; print_char '\n') g

let ic_canonical g =
  let step1 = List.map (fun (v,m,t) -> (v, ms_canonical m, t)) g in
  let cmp = (fun (va,ma,_) (vb,mb,_) -> let c1 = Char.compare va vb in if (c1 != 0) then c1 else ms_compare ma mb) in
  List.sort cmp step1

(* égalité de contextes raffinés *)
let eq_ic (g : inter_contexte) (d : inter_contexte) : bool = ( g = d)
  (* let eq = fun (v1, i1, t1) (v2, i2, t2) -> (v1 = v2) && (t1 = t2) && (eqtypemset i1 i2) in
  eqmultiset g d eq *)

(* let rec generate_ic l:((variable * inter_type_mset) list) : inter_contexte =
  match l with [] -> []
  | (v,m)::ll -> () *)

(* égalité entre décomposition (= liste/tupple de contextes) *)
let eq_dec (l1 : inter_contexte list) (l2 : inter_contexte list) : bool = 
  try List.for_all2 (eq_ic) l1 l2 with _ -> false (*cas des longueurs distinctes*)

(* si n éléments de l sont égaux, supprime les n-1 premières occurences *)
let rec delete_multiplicity (l : 'a list)  (eq : 'a -> 'a -> bool) : 'a list =
  match l with [] -> []
  | e::ll -> let r = (delete_multiplicity ll eq) in
  if (List.exists (eq e) r) then r else (e::r)

(* liste les paires de sous-multiensembles distincts formant le multiensemble donné *)
let rec list_of_multisets_pairs (m : ('a * int) list) (eq : 'a -> 'a -> bool) : (('a * int) list * ('a * int) list) list = 
  match m with [] -> [[],[]]
  | (e,n)::mm -> let p = list_of_multisets_pairs mm eq in 
    let f = (fun (ml,mr) -> ((e,n)::ml,mr)::(ml,(e,n)::mr)::(List.init (n-1) (fun i -> ((e,n-i-1)::ml,(e,i+1)::mr)))) in (*évite les éléments de forme (alpha, 0)*)
    List.concat_map f p

(* liste les paires de sous-contextes distinctes formant le contexte donné *)
let rec list_of_subcontext_pairs (g : inter_contexte) : (inter_contexte * inter_contexte) list = 
  match g with [] -> []
  | (v, m, ty) :: g -> 
      let partial_contexts = (list_of_subcontext_pairs g) in
      let multiset_pairs = (list_of_multisets_pairs m eqtype) in
      (* on ajoute chaque paire de multiset possible à chaques paires de "contexte partiel"*)
      let f = (fun (l,r) ->
        List.map (fun (ml, mr) -> (((v, ml, ty)::l), ((v, mr, ty)::r))) multiset_pairs
      ) in List.concat_map f (if (partial_contexts <> []) then partial_contexts else [([], [])])

(* liste les décompositions distinctes d'un contexte donné *)
let rec list_of_contexte_decomposition (n:int) (g:inter_contexte) : (inter_contexte list) list = 
  assert(n>0); if (n=1) then [[g]] else 
  List.concat_map (fun (l,r) -> List.map (fun e -> l :: e) (list_of_contexte_decomposition (n-1) r) ) (list_of_subcontext_pairs g)
  (* fixe le premier élément avec list_of_subcontext_pairs, et s'appelle récursivement pour définir les n-1 éléments restants *)

(* compte le nombre de décomposition de n sous contextes de g à des fins de debug *)
let contexte_decomposition_count (n : int) (g:inter_contexte) : int =
  (* on veut énumérer les f : [k] -> [n], arbitraires, avec les [k] indiscernables et les [n] oui*)
  (* = ((n : k)) = ( (n+k-1) : k) (coefficients binomiaux)*)
  (* = # of multisets of cardinal k with elements from 1..n *)
  let rec binomial n k = 
    if (n<0 || k<0 || n<k) then 0 else
    if (k=0 && n = 0) then 1 else 
    (binomial (n-1) (k-1)) + (binomial (n-1) k) 
  in
  let multiset_coefficient n k = (binomial (n+k-1) k)
    (* if (k <= 0 && n >= 0) then 1 else
    if (k >  0 && n =  0) then 0 else
      (multiset_coefficient n (k-1)) + (multiset_coefficient (n-1) k) *)
    in
  let decomposition_count_ms = List.fold_left (fun prod (_,k) -> prod * (multiset_coefficient n k)) in
  List.fold_left (fun prod (_,m,_) -> (decomposition_count_ms prod m)) 1 g

(* addition de deux contextes*)
let add_ic (g:inter_contexte) (h:inter_contexte) : inter_contexte =
  List.map2 (fun (v,m1,t) (_,m2,_) -> (v,add_mset m1 m2,t)) g h

let rec is_null (g:inter_contexte) : bool =
  match g with [] -> true
  | (_, [], _) :: gg -> is_null gg
  |  _ -> false


let test_inter_type (_:unit) : unit = 
  
  (* tests sur eqtypemset *)

  let tt:inter_type = Base true in
  let ff:inter_type = Base false in
  let m1 = create_ms [(tt, 5) ; (ff, 5)] in
  let m2 = create_ms [(ff, 5) ; (tt, 5)] in
  let m3 = create_ms [(ff, 6) ; (ff, 4)] in
  let m4 = create_ms [(tt, 10) ; (ff, 15)] in
  let m5 = create_ms [(ff, 15) ; (tt, 10)] in

  assert  (eqtypemset m1 m1);
  assert  (eqtypemset m1 m2);
  assert  (eqtypemset m2 m1);
  assert (not (eqtypemset m1 m3));
  assert (not (eqtypemset m2 m3));
  assert (not (eqtypemset m3 m4));
  assert  (eqtypemset m2 m1);
  assert  (eqtypemset m4 m5);

  (* tests sur eqtype *)
  
  let a = create (Map ([(tt, 3) ; (ff, 1)], tt)) in
  let b = create (Map ([(ff, 1) ; (tt, 3)], tt)) in
  assert (eqtype a b);
  assert (not (eqtype a tt));
  assert (not (eqtype ff b));
  let c = create (Map ([(a, 2) ; (b, 3)], a)) in
  let d = create (Map ([(b, 3) ; (a, 2)], b)) in
  assert (eqtype c d);
  assert (not (eqtype c a));
  assert (not (eqtype d tt));
  let e = create (Map ([(a, 2) ; (b, 3)], tt)) in
  assert (not (eqtype e d));
  let f = create (Map ([(b, 2) ; (tt, 1)], a)) in
  assert (not (eqtype c f));

  (* tests sur print_it *)

  print_it tt ; print_char '\n';
  print_it ff ; print_char '\n';
  print_it a ; print_char '\n';
  print_it b ; print_char '\n';
  print_it c ; print_char '\n';
  print_it d ; print_char '\n';
  print_it e ; print_char '\n';
  print_it f ; print_char '\n';

  (* tests sur eq_ic *)

  let g = ic_canonical [('a', [(a,1)] , Func (Base, Base)) ; ('x', [(tt, 2) ; (ff, 1)] , Base) ; ('c', [(c,2);(d,1)] , Func (Func (Base, Base), Func (Base, Base))) ] in
  let h = ic_canonical [('x', [(ff,1);(tt,2)] , Base) ; ('a', [(a,1)] , Func (Base, Base)) ; ('c', [(d,1);(c,2)] , Func (Func (Base, Base), Func (Base, Base))) ] in
  assert (eq_ic [] []);
  assert (eq_ic g g);
  assert (not(eq_ic g []));
  assert (eq_ic g h);

  (* tests sur print_ic *)
  
  let z = ic_canonical [('a', [] , Func (Base, Base)) ; ('x', [] , Base) ; ('c', [] , Func (Func (Base, Base), Func (Base, Base))) ] in
  print_string "contexte g : \n" ; print_ic g ; print_char '\n';
  print_string "contexte h : \n" ; print_ic h ; print_char '\n';
  print_string "contexte [] : \n" ; print_ic [] ; print_char '\n';
  print_string "contexte z : \n" ; print_ic z ; print_char '\n';

  (* tests sur add_ic *)

  assert (eq_ic [] (add_ic [] []));
  assert (eq_ic g (add_ic h z));
  assert (eq_ic (add_ic g h) (add_ic h g));
  assert (eq_ic (add_ic g (add_ic h z)) (add_ic h g));
  assert (not (eq_ic (add_ic g (add_ic g g)) (add_ic h z)));

  (* tests sur list_of_contexte_decomposition *)
  let rec list_distinct (l : 'a list) (eq : 'a -> 'a -> bool) : bool =
    (* teste si les éléments de l sont distincts *)
    match l with [] -> true
    | e :: ll -> (not (List.exists (eq e) ll)) && (list_distinct ll eq)
  in 
  let sum_ic = (List.fold_left (add_ic) z) in
  for n = 1 to 5 do
    (* se somment bien à g *)
    let decomp = list_of_contexte_decomposition n g in
 (*    List.iter (fun clist -> List.iter (fun ic -> print_ic ic ; print_string "+++++") clist ; print_string "\n-----------------------\n") decomp ; *)
    print_string ("somme pour n = "); print_int n; print_char '\n'; 
    (List.iter (fun l -> assert (eq_ic (sum_ic l)  g)) decomp);
    (* sont distincts*)
    print_string ("distinction pour n = "); print_int n; print_char '\n';
    assert (list_distinct decomp eq_dec);
    (* sont tous présents *)
    print_string "comparaison de cardinal : ";
    print_int (contexte_decomposition_count n g); print_string " VS ";
    print_int (List.length decomp); print_char '\n';
    assert ((contexte_decomposition_count n g) = List.length decomp)
  done;