open Inter_type

val dagger : Termes.t -> inter_contexte -> inter_type_mset -> r

val eval : Termes.t -> inter_contexte -> inter_type -> r

val test_wrel : unit -> unit

val bench_all : unit -> unit