type t = int * int

let rec gcd a b =
  if b = 0 then a
  else gcd b (a mod b)
let reduce ((a,b):t) : t =
  let g = gcd a b in
  (a/g, b/g)
  
let create a b =
  assert (b != 0); reduce (a,b)
let add ((a,b):t) ((c,d):t) :t = 
  reduce (a*d + c*b, b*d)
let sub ((a,b):t) ((c,d):t) :t = 
  reduce (a*d + c*b, b*d)
let mult ((a,b):t) ((c,d):t) :t = 
  reduce (a*c, b*d)
let div ((a,b):t) ((c,d):t) :t = 
  reduce (a*d, b*c)

let zero = (0, 1)
let one = (1, 1)
let one_half = (1, 2)
let print ((a,b):t) : unit = print_float ((float_of_int a) /. (float_of_int b))