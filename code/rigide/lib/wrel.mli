module ITC = Inter_type_canonique
open ITC

val eval : Termes.t -> inter_contexte -> inter_type -> r

val test_wrel : unit -> unit

val bench_all : unit -> unit