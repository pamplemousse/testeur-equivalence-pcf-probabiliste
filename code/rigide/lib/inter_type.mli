type inter_type =
  | Base of bool
  | Map of (inter_type_mset * inter_type)

and inter_type_mset = (inter_type) list (*invariant : l'entier est >0 *)

val card : inter_type_mset -> int

val print_it : inter_type -> unit

val eqtype : inter_type -> inter_type -> bool

val eqtypemset : inter_type_mset -> inter_type_mset -> bool


open Termes

type r = Rationals.t
(* type variable = char
type ty = Termes.ty (* | Base | Func of (ty * ty) *) *)

val print_t : ty -> unit
(* 
val raffinable : inter_type -> bool (* renvoie true ssi raffine n'échoue pas *)

val raffine : inter_type -> ty -> bool
 *)
(* contexte raffiné par des types intersections *)
type inter_contexte = (variable * inter_type_mset * ty) list (*hyp : si (x, a, ty), raffine a ty = true*)

val print_ic : inter_contexte -> unit

(* val generate_ic : ((variable * inter_type_mset) list) -> inter_contexte option *)

val eq_ic : inter_contexte -> inter_contexte -> bool

val list_of_contexte_decomposition : int -> inter_contexte -> (inter_contexte list) list

val eq_dec : inter_contexte list -> inter_contexte list -> bool

val delete_multiplicity : 'a list -> ('a -> 'a -> bool) -> 'a list

val is_null : inter_contexte -> bool

val ic_canonical : inter_contexte -> inter_contexte

val test_inter_type : unit -> unit