type variable = char

type ty = | Base | Func of (ty * ty)

let rec print_ty (a : ty) = match a with Base -> print_char 'B'
  | Func (b, c) -> print_char '(' ; print_ty b ; print_string "→" ; print_ty c ; print_char ')'

(*On identifie les termes et leur typage*)
type terme =
  | TT
  | FF
  | Bot
  | Var of variable
  | Abs of (variable * ty * terme)
  | App of (terme * terme)
  | If of (terme * terme * terme)
  | Choice of ((Rationals.t * terme) list) (*hyp : les p_i sont >=0 et se somment à <= 1*)

let rec print_terme (t: terme) =
  match t with
  | TT -> print_string "tt"
  | FF -> print_string "ff"
  | Bot -> print_string "⊥"
  | Var v -> print_char v
  | Abs (v, ty, a) -> print_string "(𝜆" ; print_char v; print_string ":" ; print_ty ty; print_char '.' ; print_terme a; print_char ')'
  | App (a,b) -> print_terme a ; print_char '(' ; print_terme b ; print_char ')' 
  | If (a, b, c)-> print_string "if (" ; print_terme a ; print_string ") then (" ; print_terme b ; print_string ") else (" ; print_terme c ; print_string ")"
  | Choice l ->  if (l = []) then print_string "Ω" else
    (print_string "("; List.iter (fun (q, a) -> Rationals.print q; print_string "("; print_terme a; print_string ")⊕ ") l; print_string "\b\b\b))")

type contexte = (variable * ty) list

let print_c = List.iter (fun (v,t) -> print_char v ; print_string " : " ; print_ty t ; print_string " | ") 

type jugement = (contexte * terme * ty)

let print_j ((c,m,t):jugement) = print_c c; print_string " ⊢ " ; print_terme m ; print_string " : " ; print_ty t ; print_string "\n"

(* attempt to fuse two contexts g and h (if they're compatible) *)
(* TODO : smarter implementation by sorting according to variables, then merging *)
let rec fuse_contexte (g : contexte) (h : contexte) : contexte =
  match g with
  | [] -> h
  | (v,tv)::gg -> 
    if (List.exists (fun (x,tx) -> (x = v) && (tx <> tv)) h) then failwith "fusion de contextes incompatibles"
    else (v,tv)::(fuse_contexte gg (List.filter (fun (x,_) -> x <> v) h)) 

(*identifie une séquence de la forme ((vM)N...)P et applique f à M, N,... P*)
let find_sequence (a : terme) (f : terme -> 'a): (variable * 'a list) =
  let rec aux  (res : 'a list) (a : terme) : (variable * 'a list) =
    match a with
    | App (b, c) -> aux ((f c)::res) b
    | Var v -> (v, res)
    | _ -> raise (Failure "ceci n'est pas une séquence")
  in aux [] a

(*construit un jugement sur a "étendant" le contexte g
  on suppose m sous forme normale, avec les variales libres de m présentes dans g *)
let rec extend_contexte (m:terme) (g:contexte) : jugement =
  match m with
  | TT | FF-> (g, m, Base)
  | Bot -> failwith "Bot n'est pas typable"
  | Abs (v, tv, a) -> let (ca, _, ta) = extend_contexte a ((v,tv)::g) in 
    (List.filter (fun (x,_) -> (x<>v)) ca, m, Func (tv, ta)) (* on retire l'hypothèse v:tv *)
  | App (a,b) -> 
    let (ca, _, ta) = extend_contexte a g in
    let (cb, _, tb) = extend_contexte b g in begin
    match ta with
    | Base -> failwith "application mal typée"
    | Func (tin, tout) ->
      if (tin <> tb) then failwith "application mal typée"
      else ((fuse_contexte ca cb), m, tout)
    end
  | If (a, b, c)-> begin
    let (ca, _, ta) = extend_contexte a g in
    let (cb, _, tb) = extend_contexte b g in
    let (cc, _, tc) = extend_contexte c g in
    if ((ta <> Base) || (tb != tc)) then failwith "if/then/else mal typé"
    else ((fuse_contexte ca (fuse_contexte cb cc)), m, tb)
    end
  | Choice l -> let ctl = List.map (fun (_,a) -> extend_contexte a g) l in
    begin match ctl with
    | [] -> ([],m,Base)
    | (ca, _, ta)::ctl -> if (List.for_all (fun (_,_,tb) -> ta=tb) ctl) 
      then ((List.fold_left fuse_contexte ca (List.map (fun (c,_,_) -> c) ctl)), m, ta) 
      else failwith "Choice mal typé"
    end
    
  | Var v -> let (_, tv) = (List.find (fun (x,_) -> x=v) g) in (g, m, tv)

(* extrait un contexte qui type les variables libres du terme m *)
let contexte_of_terme (m:terme) : contexte = 
  (* hyp : m est sous forme normale*)
  let rec aux (m:terme) (contexte_liees : contexte) (req_type : ty) : contexte =
    (match m with
    | TT | FF-> []
    | Bot -> failwith "Bot n'est pas typable"
    | Abs (v, tv, a) -> (aux a ((v,tv)::contexte_liees) (Func (tv, req_type)))
    | App _ -> 
      let (v, argl) = (find_sequence m (fun x -> x)) in 
      let (last_ty,c_list) = (List.fold_left_map 
        (fun t_accu mi -> 
          let ti = (get_type mi contexte_liees) in
          let gi = (aux mi contexte_liees ti) in
          (Func (ti, t_accu),gi)
        ) Base (List.rev argl)) in
        (List.fold_left fuse_contexte (fuse_contexte ((v, last_ty)::[]) contexte_liees) c_list)
    | If (a, b, c)-> fuse_contexte (aux a contexte_liees Base) (fuse_contexte (aux b contexte_liees req_type) (aux c contexte_liees req_type))
    | Choice l -> begin match l with
      | [] -> []
      | (_,a)::l -> List.fold_left (fun c (_,b) -> fuse_contexte (aux b contexte_liees req_type) c) (aux a contexte_liees req_type) l
      end
    | Var v -> begin
        if (List.exists (fun (x,_) -> v = x) contexte_liees) then [] (* si v est liée *)
        else (v, req_type)::[]
      end)
  (*Ne fonctionne pas si appellée sur le membre d'une application*)
  and get_type (m:terme) (g: contexte) : ty =
    match m with
    | TT | FF-> Base
    | Bot -> failwith "Bot n'est pas typable"
    | Abs (v, t, a) -> Func (t, get_type a ((v,t)::g))
    | If (_, b, _)-> get_type b g
    | Choice l -> begin match l with
    | [] -> Base
    | (_,a)::_ -> get_type a g
  end 
    | App _ -> Base
    | Var v -> begin
      match (List.find_opt (fun (x,_) ->  (x = v)) g) with | None -> Base (*v est libre, mais tout à droite d'un sous arbre (pas éta-expansé) : type bool*)
      | Some (_,tv) -> tv
    end
  in (aux m [] (get_type m []))

let jugement_of_terme a = extend_contexte a (contexte_of_terme a)
(* d'après les hypothèses si dessous, si a est sous forme normale, n'échoue jamais *)

let cloture a =
  let c = contexte_of_terme a in
  List.fold_left (fun a (v,t) -> Abs (v, t, a)) a (List.rev c)

(*preuve de neutralité/normalité d'un terme, qu'on peut identifier au terme lui-même*)
type neutre = 
  | App_ne of (variable * normal list) (* la neutralité est représentée ainsi pour faciliter l'évaluation *)
  (* alternative : constructeurs Var et App, avec GADT pour forcer les contraintes de types *)
(* 
type _ ty_ga =
  | Base_ga : bool ty_ga
  | Fun_ga : (('a ty_ga) * ('b ty_ga)) -> ('a -> 'b) ty_ga
*)
and normal = 
  | TT_no
  | FF_no
  | Neutre_no of neutre (* /!\ invariant : doit avoir un type booléen *)
  | Abs_no of (variable * ty * normal)
  | If_no of (neutre * normal * normal) (* /!\ invariant : doit avoir un type booléen *)
  | Choice_no of ((Rationals.t * normal) list) (*hyp : les p_i sont >=0 et se somment à <= 1*)
  
type t = normal
let rec terme_of_neutre (a : neutre) : terme = match a with App_ne (v, l) ->
  List.fold_left (fun accu e -> App (accu, terme_of_normal e)) (Var v) l
and terme_of_normal (a : normal) : terme = 
  match a with
  | TT_no -> TT
  | FF_no -> FF
  | Neutre_no n -> terme_of_neutre n
  | Abs_no (v, t, n) -> Abs (v, t, terme_of_normal n)
  | If_no (m, n, p) -> If (terme_of_neutre m, terme_of_normal n, terme_of_normal p)
  | Choice_no l -> Choice (List.map (fun (q,n) -> (q, terme_of_normal n)) l)

(* hypothèse : les termes en entrée sont sous forme normale *)
let rec neutre_of_terme_typed (a : terme) : neutre * ty =
  let (v, nl) = find_sequence a normal_of_terme in (App_ne (v, nl), Base) (*/!\ : aucun check de type n'est fait (peut être fait en amont)*)
and normal_of_terme_typed (a : terme) : normal * ty =
  match a with
  | TT -> (TT_no, Base)
  | FF -> (FF_no, Base)
  | Abs (v, t, a) -> let (n,tres) = normal_of_terme_typed a in (Abs_no (v, t, n), Func (t, tres))
  | If (a, b, c)-> 
    let (na,ta) = neutre_of_terme_typed a in
    if (ta = Base) then 
      let (nb,tb) = normal_of_terme_typed b in
      let (nc,tc) = normal_of_terme_typed c in
      assert (tb = tc);
      (If_no (na, nb, nc), tb)
    else failwith "la condition du if non neutre"
  | Choice l -> let qntl = List.map (fun (q, a) -> (q, normal_of_terme_typed a)) l in
    begin match qntl with
    | [] -> (Choice_no [], Base)
    | (_, (_, ta))::_ -> if (List.for_all (fun (_,(_,tb)) -> ta=tb) qntl) 
      then (Choice_no (List.map (fun (q,(n,_)) -> (q,n)) qntl), ta)
      else failwith "Choice mal typé"
    end
  | Var _ -> let (na,ta) = neutre_of_terme_typed a in (Neutre_no na, ta)
  | App _ ->
    let (na,ta) = neutre_of_terme_typed a in
    if (ta = Base) then (Neutre_no na, ta) else (print_terme a ; print_ty ta ; failwith "le terme n'est pas pleinement éta expansé")
  | Bot -> failwith "Ce terme n'est pas sous forme normale"
and neutre_of_terme a = let (n,_) = neutre_of_terme_typed a in n
and normal_of_terme a = let (n,_) = normal_of_terme_typed a in n 

let choice2 (m, n) = Choice [(Rationals.one_half,m) ; (Rationals.one_half, n)]
let test_termes _ = 
  let m1:terme = App (App (App (Var 'x', TT), FF), FF) in 
  let m2:terme = App (App (App (App (App (App (Var 'y', TT), FF), FF), TT), FF), FF) in 
  let m3:terme = App (App (App (Var 'x', App (App (App (Var 'x', TT), FF), FF)), FF), FF) in
  let m4:terme = Abs ('x', (Func (Base, (Func (Base, (Func (Base, Base)))))), m1) in
  let m5:terme = If (m2, choice2 (m1, m3), m3) in

  (* test of print_terme *)
  print_terme m1; print_char '\n';
  print_terme m2; print_char '\n';
  print_terme m3; print_char '\n';
  print_terme m4; print_char '\n';
  print_terme m5; print_char '\n';

  (* test of jugement_of_terme and print_j *)

  print_j (jugement_of_terme m1); print_char '\n';
  print_j (jugement_of_terme m2); print_char '\n';
  print_j (jugement_of_terme m3); print_char '\n';
  print_j (jugement_of_terme m4); print_char '\n';
  print_j (jugement_of_terme m5); print_char '\n';

  (try (let _ = jugement_of_terme (Abs ('x', Base, App (Var 'x', Var 'x'))) in ()) with _ -> print_string "failed successfully.\n"); 
  

  (* test of normal_of_terme and terme_of_normal *)
  assert ((terme_of_normal (normal_of_terme m1)) = m1);
  assert ((terme_of_normal (normal_of_terme m2)) = m2);
  assert ((terme_of_normal (normal_of_terme m3)) = m3);
  assert ((terme_of_normal (normal_of_terme m4)) = m4);
  assert ((terme_of_normal (normal_of_terme m5)) = m5);
  (try (let _ = normal_of_terme (If ((If (Var 'x', TT, FF), TT, FF))) in ()) with _ -> print_string "failed successfully.\n"); 
  (try (let _ = normal_of_terme (App (Abs ('x', Func(Base,Base), App(Var 'x', TT)), Abs ('y', Base, FF))) in ()) with _ -> print_string "failed successfully.\n"); 

  (* test of neutre_of_terme and terme_of_neutre *)
  assert ((terme_of_neutre (neutre_of_terme m1)) = m1);
  assert ((terme_of_neutre (neutre_of_terme m2)) = m2);
  assert ((terme_of_neutre (neutre_of_terme m3)) = m3);
  (try (let _ = neutre_of_terme m4 in ()) with _ -> print_string "failed successfully.\n"); 
  (try (let _ = neutre_of_terme m5 in ()) with _ -> print_string "failed successfully.\n"); 
