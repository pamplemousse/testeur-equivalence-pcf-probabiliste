type variable = char

type ty = | Base | Func of (ty * ty)

(*On identifie les termes et leur typage*)
type terme =
  | TT
  | FF
  | Bot
  | Var of variable
  | Abs of (variable * ty * terme)
  | App of (terme * terme)
  | If of (terme * terme * terme)
  | Choice of ((Rationals.t * terme) list) (*hyp : les p_i sont >=0 et se somment à <= 1*)

val print_terme : terme -> unit

val print_ty : ty -> unit

type contexte = (variable * ty) list

val print_c : contexte -> unit

type jugement = (contexte * terme * ty)

val print_j : jugement -> unit

val jugement_of_terme : terme -> jugement

val cloture : terme -> terme

type neutre = 
  | App_ne of (variable * normal list) (* la neutralité est représentée ainsi pour faciliter l'évaluation *)  
and normal = 
  | TT_no
  | FF_no
  | Neutre_no of neutre (* /!\ invariant : doit avoir un type booléen *)
  | Abs_no of (variable * ty * normal)
  | If_no of (neutre * normal * normal) (* /!\ invariant : doit avoir un type booléen *)
  | Choice_no of ((Rationals.t * normal) list) (*hyp : les p_i sont >=0 et se somment à <= 1*)

val normal_of_terme : terme -> normal

val terme_of_normal : normal -> terme

val choice2 : terme * terme -> terme 

val test_termes : unit -> unit

type t = normal