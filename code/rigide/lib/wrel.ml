
open Inter_type
open Termes

(*fonctions auxiliaires utiles pour l'évaluation*)
let extract_f_ty (f: variable) (g : inter_contexte) : inter_type list = 
  let (_,m,_) = List.find (fun (v,_,_) -> f=v) g in m

let gamma_minus_delta (f: variable) (d : inter_type) (g : inter_contexte) : inter_contexte =
  let rec remove_iter (l : 'a list) : 'a list =
    match l with
    | [] -> failwith "d absent de a (avec f:a dans g)"
    | v::ll -> if (d=v) then ll else (v::(remove_iter ll))
  in
  (* hyp : pour un certain a, f:a est présent dans g et d et présent dans a *)
  let rec aux (g : inter_contexte) : inter_contexte =
    match g with
    | [] -> failwith "f absent de g"
    | (v,a,t)::gg -> 
      if (v <> f) then (v,a,t)::(aux gg)
      else (v, (remove_iter a), t)::gg
  in aux g

(* a1 -> ... an -> alpha => ([an ; ... ; a1], alpha)*)
let arglist_of_type (t : inter_type) (n : int) : (inter_type_mset list * inter_type) =
  let rec aux (t : inter_type) (res : inter_type_mset list ) (n:int): (inter_type_mset list * inter_type) =
    if (n=0) then (res, t) else
    match t with 
    | Base _ -> failwith "pas assez d'arguments"
    | Map (m, a) -> aux a (m::res) (n-1)
  in aux t [] n

(* let rec ty_id (t:Termes.ty) : ty = match t with |Base -> Base | Func(a,b) -> Func(ty_id a, ty_id b) *)
let tt:inter_type = Base true
let ff:inter_type = Base false
let fold_left3 f accu a b c = List.fold_left2 (fun accu fc c -> fc accu c) accu (List.map2 (fun g h -> fun e -> f e g h) a b) c
 
(* let rec print_neutre (Termes.App_ne (f,l)) : unit = print_char f ; 
  if (l <> []) then (print_string "(" ; List.iter (fun n -> print_normal n ; print_string " ; ") l ; print_string "\b\b\b)")
and print_normal (t:Termes.t) : unit =
  match t with
  | TT_no -> print_string "tt";
  | FF_no ->  print_string "ff";
  | Abs_no (x, tx, m) -> print_string "(𝜆" ; print_char x; print_string ":" ; Termes.print_ty tx; print_char '.' ; print_normal m; print_char ')'
  | If_no (a, b, c)  -> print_string "if (" ; print_neutre a ; print_string ") then (" ; print_normal b ; print_string ") else (" ; print_normal c ; print_string ")"
  | Choice_no l -> print_string "() "; List.iter (fun (q, a) -> Rationals.print q; print_string ".("; print_normal a; print_string ")⊕") l; print_string "\b )";
  | Neutre_no (ne) -> print_neutre ne *)

let rec eval  (t:Termes.t) (g:inter_contexte) (a:inter_type) : r =
  match t with
  | TT_no -> if (is_null g) && (a = Base true) then Rationals.one else Rationals.zero
  | FF_no -> if (is_null g) && (a = Base false) then Rationals.one else Rationals.zero
  | Abs_no (x, tx, m) -> begin
    match a with 
    | Base _ -> Rationals.zero
    | Map (alpha, b) -> eval m ((x,alpha, tx)::g) b
    end
  | If_no (cond, n, m)  ->let decomp = (list_of_contexte_decomposition 2 g) in
    List.fold_left (fun sum d -> match d with
      | [g1 ; g2] -> Rationals.add sum (Rationals.add 
          (Rationals.mult (eval_neutre cond g1 tt) (eval n g2 a)) 
          (Rationals.mult (eval_neutre cond g1 ff) (eval m g2 a)))
      | _ -> failwith "mauvaise décomposition"
    ) Rationals.zero decomp
  | Choice_no l -> List.fold_left (fun sum (q, m) -> Rationals.add sum (Rationals.mult q (eval m g a))) Rationals.zero l
  | Neutre_no (ne) -> eval_neutre ne g a

and eval_neutre (t:Termes.neutre) (g:inter_contexte) (a:inter_type) : r =
  match t with App_ne(f, tlist) ->
  let alpha = extract_f_ty f g in let n = List.length tlist in
  if (n = 0) then (
    if ((match a with | Base _ -> true | _ -> false) && 
      (List.for_all (fun (v, m, _) ->  if (v = f) then m = [a] else m = []) g) ) then Rationals.one else Rationals.zero)
  else
    if (alpha = []) then Rationals.zero
    else let delta = Option.get (List.fold_left (fun _ d -> Some d) None alpha) in
    let (arglist, b) = arglist_of_type delta n in 
    if (b <> a) then Rationals.zero else
    let decomplist = list_of_contexte_decomposition n (gamma_minus_delta f delta g) in
    (* somme sur les decomposition de gamma_prime = gamma "- delta" *)
    List.fold_left (fun sum2 decomp ->
      (* on ajoute le produit sur les Mi*)
      Rationals.add sum2 (fold_left3 (fun prod g_i a_i m_i-> Rationals.mult prod (dagger m_i g_i a_i)) Rationals.one decomp arglist tlist)
      ) Rationals.zero decomplist

and dagger (t:Termes.t) (g:inter_contexte) (m:inter_type_mset) : r = 
  let n = card m in 
  if (n = 0) then (if (is_null g) then Rationals.one else Rationals.zero) else
  let decomp = list_of_contexte_decomposition n g in
  List.fold_left (* somme sur les décomposition de g*)
    (fun sum glist ->
      Rationals.add sum
      (List.fold_left2 (fun prod gamma alpha -> Rationals.mult prod (eval t gamma alpha)) Rationals.one glist m)
    )
    Rationals.zero
    decomp

let sum_proof_weight = eval

(*------------------------------------------------------------------------------------------------------*)

module ITC = Inter_type_canonique

(*
let rec rigide_of_canonique (a:ITC.inter_type) : inter_type =
  match a with
  | Base b -> Base b
  | Map (m,b) -> Map (ms_rigide_of_canonique m, rigide_of_canonique b)
and ms_rigide_of_canonique (m:ITC.inter_type_mset) : inter_type_mset =
  let m1 = List.map (fun (a, n) -> (rigide_of_canonique a, n)) m in
  (List.concat_map (fun (a,n) -> List.init n (fun _ -> a)) m1) 

let ic_rigide_of_canonique (g:ITC.inter_contexte) : inter_contexte =
  List.map (fun (v, m, t) -> (v, ms_rigide_of_canonique m, t)) g *)

(* given [l1 ; ... ; ln], list all possible [a1 ; ... ; an] such that ai is in li *)
let rec cartesian_product (l : 'a list list) : 'a list list =
  match l with
  | [] -> [[]]
  | li::ll -> let next = cartesian_product ll in 
  List.concat_map (fun a -> List.map (fun l -> a::l ) next) li

(* for m a multiset, give the array of value in m, a, and the list of lists [i1 ; ... ; ik] 
  such that the [a.(i1) ; ... ; a.(ik)] describe all the way of shuffling m *)
let seqlist_of_mult (m : ('a * int) list) : ('a array) * (int list list) =
  let a = Array.of_list (List.map (fun (a,_) -> a ) m) in
  (*list all the ways to add k times the element "i" to the list l*)
  let rec place (k:int) (i:int) (l: int list) : int list list = 
    if (k<=0) then [l] else
    let before = (*adds an instance of "i" at the begining of l*)
      (List.map (fun d -> i::d) (place (k-1) i l)) in
    let after = (*add all instances of "i" inside of l*)
    match l with
    | [] -> [] (*"before" already describes all the possible vallues*)
    | j::ll -> List.map (fun t -> j::t) (place k i ll)
    in before @ after
  in
  (*list the sequences representing a "shuffle" of m, while starting with the index "count"*)
  let rec aux (m : ('a * int) list) (count : int): int list list =
    match m with
    | [] -> [[]]
    | (_, k)::mm -> List.concat_map (fun l -> place k count l) (aux mm (count+1))
  in (a, aux m 0)

let rec pos (a:ITC.inter_type) : inter_type list = 
  match a with
  | Base b ->  [Base b]
  | Map (m, b) -> let mneg = ms_neg m in let bpos = pos b in
    List.concat_map (fun beta -> List.map (fun m -> Map (m, beta)) mneg) bpos

and neg (a:ITC.inter_type) : inter_type list =
  match a with
  | Base b ->  [Base b]
  | Map (m, b) -> let mpos = ms_pos m in let bneg = neg b in
    List.concat_map (fun beta -> List.map (fun m -> Map (m, beta)) mpos) bneg

and ms_pos (a:ITC.inter_type_mset) : inter_type_mset list = 
  let poslist_mult = List.map (fun (a,n) -> (pos a,n)) a in
  let poslist_seq = List.concat_map (fun (pl, n) -> List.init n (fun _ -> pl)) poslist_mult in
  cartesian_product poslist_seq
  
and ms_neg (a:ITC.inter_type_mset) : inter_type_mset list =
  let (tarr, intseqlist) = seqlist_of_mult a in
  let neglist_array = Array.map neg tarr in
  (* list all the ways to replace the elements j in l by an element alpha of neglist_array.(j)*)
  let rec enum_replacement (l : int list) : inter_type list list = 
    match l with
    | [] -> [[]]
    | j::ll -> List.concat_map (fun r ->
        List.map (fun alpha -> alpha :: r) (neglist_array.(j))
      ) (enum_replacement ll)
  in List.concat_map enum_replacement intseqlist

let ic_neg (g:ITC.inter_contexte) : inter_contexte list =
  let rec aux (g:ITC.inter_contexte) : inter_contexte list =
    match g with 
    | [] -> [[]]
    | (v, m, t)::gg -> List.concat_map (fun ic ->
        (List.map (fun ms -> (v,ms,t)::ic) (ms_neg m))
      ) (aux gg)
  in aux g

(* let ic_pos (g:ITC.inter_contexte) : inter_contexte list =
  let rec aux (g:ITC.inter_contexte) : inter_contexte list =
    match g with 
    | [] -> [[]]
    | (v, m, t)::gg -> List.concat_map (fun ic ->
        (List.map (fun ms -> (v,ms,t)::ic) (ms_pos m))
      ) (aux gg)
  in aux g *)
(*------------------------------------------------------------------------------------------------------*)

let eval (t:Termes.t) (g:ITC.inter_contexte) (a:ITC.inter_type) : r =
  List.fold_left (fun sum1 ic ->
      List.fold_left (fun sum2 it ->
        Rationals.add sum2 (sum_proof_weight t ic it)
      ) sum1
      (pos a)
    ) Rationals.zero
    (ic_neg g)

(*------------------------------------------------------------------------------------------------------*)

(* approche par mémoisation : on retient l'ensemble des contextes et types d'une taille k donnée, à mesure que k croit *)
(*(k,t) -> liste des a◁t figurant des multisets de cardinaux <= k  *)
let it_list_table : ((int * ty),  ITC.inter_type list) Hashtbl.t = Hashtbl.create 256 
(*(k,n,t) -> liste des multisets m de cardinal au plus n muni d'élements de it_list_table (k,t) *)
let ms_it_list_table : ((int * int * ty), ITC.inter_type_mset list) Hashtbl.t = Hashtbl.create 256 

(* génére la liste des listes l de taille n, dont les éléments (>=0) somment à k *)
let generate_decompositions (k:int) (n:int) : int list list = 
  (* aux génère les décompositions de taille n sommant à <= k *)
  let rec aux (n:int) : (int list * int) list = 
    if (n<=0) then [([], 0)] else
    let last = aux (n - 1) in
    List.concat_map (fun (l, s) ->
      List.init (k-s+1) (fun i -> (i::l,s+i))
    ) last
  in List.filter_map (fun (l,v) -> if (v=k) then Some l else None) (aux n)  

let rec generate_it_list (k : int) (t : ty):  ITC.inter_type list = 
  match (Hashtbl.find_opt it_list_table (k,t)) with
  | Some l -> l
  | None -> 
    let res:  ITC.inter_type list = begin 
    match t with
    | Base -> [Base true ; Base false]
    | Func (a,b) ->
        let a_list = generate_ms_it_list k k a in
        let b_list = generate_it_list k b in
        List.concat_map (fun e -> (List.map (fun m ->  ITC.Map (m,e)) a_list)) b_list
    end in
    Hashtbl.replace it_list_table (k,t) res;
    res

and generate_ms_it_list (k : int) (n : int) (t : ty):  ITC.inter_type_mset list = 
  match (Hashtbl.find_opt ms_it_list_table (k,n,t)) with
  | Some l -> l
  | None -> 
    let res = begin 
      if (n<=0) then [[]]
      else
        let inf = generate_ms_it_list k (n-1) t in
        let sup = generate_ms_it_list_exact k n t in
        List.append sup inf
    end in
    Hashtbl.replace ms_it_list_table (k,n,t) res;
    res

and generate_ms_it_list_exact (k : int) (n : int) (t : ty):  ITC.inter_type_mset list = 
  if (n<=0) then [[]] else
  let vlist = generate_it_list k t in
  let c = List.length vlist in
  let decomp = generate_decompositions n c in
  List.map (fun indices -> List.fold_left2 (fun stack i t -> if (i = 0) then stack else (t,i)::stack) [] indices vlist) decomp

(* liste les contextes utilisant des éléments de taille jusqu'à k, dont les cardinaux sont au plus k -> présente potentiellement des doublons *)
let generate_ic_list (k : int) (gamma : Termes.contexte):  ITC.inter_contexte list = 
  let rec aux (gamma : Termes.contexte) (msetll :  ITC.inter_type_mset list list) :  ITC.inter_contexte list =
    match (gamma, msetll) with
    | ([],[]) -> [[]]
    | ((v,t)::gg, mslist::next) -> List.concat_map (fun c -> List.map (fun m -> (v,m,t)::c) mslist) (aux gg next)
    | _ -> failwith "cas impossible"
  in aux gamma (List.map (fun (_, t) -> (generate_ms_it_list k k t)) gamma)

exception FoundDifference of ( ITC.inter_contexte * ITC.inter_type)
let contextual_equivalence_aux (m:Termes.t) (n:Termes.t) (gamma : contexte) (t : ty): ( ITC.inter_contexte * ITC.inter_type) option =
  try let rec aux (k:int) (iclist :  ITC.inter_contexte list) (itlist :  ITC.inter_type list) : unit =
    assert (k<4);
    match iclist with
    | [] -> aux (k+1) (generate_ic_list (k+1) gamma) (generate_it_list (k+1) t)
    | g::ll -> List.iter (fun a -> 
      if ((eval m g a) <> (eval n g a)) 
      then (raise (FoundDifference (g,a)))) itlist; 
      (aux k ll itlist)
  in (aux 0 (generate_ic_list 0 gamma) (generate_it_list 0 t)); None
  with | FoundDifference d -> Some d

let contextual_equivalence (m:terme) (n:terme) : (ITC.inter_contexte * ITC.inter_type) option =
  (*hypothèse : m et n sont sous forme normale*) 
  let (gm, _, tm) = jugement_of_terme m in
  let (gn, _, _) = jugement_of_terme n in
  let m_no = normal_of_terme m in
  let n_no = normal_of_terme n in
  contextual_equivalence_aux m_no n_no (if (gm <> gn) then (delete_multiplicity (gn @ gm) (fun (v,_) (x,_) -> v=x)) else gm) tm
  (*Si gm et gn typent des variables différement, ou que tm <> tn, les types dictés par m priment *)

let variable_iterator = ref 'A'
let next_variable (_:unit) = let c = ! variable_iterator in 
  variable_iterator := char_of_int (1 + (int_of_char c)); c
let formal_parameters (n : int) : Rationals.t list= (*liste de n rationnels se sommant dans [0..1]*)
  let smax = 536870912 in (*2^29*)
  let plist = ref (List.init n (fun _ -> Random.int (smax/n))) in
  let q = ref (1 + (Random.int (smax-1))) in
  while (!q < (List.fold_left (+) 0 !plist)) do (*tant que la somme des p sur q est > 1*)
    plist := (List.init n (fun _ -> Random.int (smax/n)));
    q := (1 + (Random.int (smax-1)))
  done;
  List.map (fun p -> Rationals.create p !q) !plist

(* tiré de Probabilistic coherence spaces are fully abstract for probabilistic PCF *)
let contexte_separant (a : ITC.inter_type) (t : ty) : terme =
  (* hyp : a ◁ t*)
  variable_iterator := 'A';
  let omega = Choice [] in 
  let and_terme (a:terme) (b:terme) = If (b, a, FF) in
  let and_iter (l: terme list) = 
    match l with
    | [] -> TT
    | m :: ll -> List.fold_left and_terme m ll
  in
  let rec pfun (a : ITC.inter_type) (t : ty) : terme =
    let nextvar = next_variable () in 
    match a with
    | Base true ->  Abs (nextvar, Base, If (Var nextvar, TT, omega)) 
    | Base false -> Abs (nextvar, Base, If (Var nextvar, omega, TT)) 
    | Map (b, c) -> begin
      match t with
      | Func(bt, ct) -> 
        let bflat = List.concat_map (fun (a, n) -> List.init n (fun _ -> nfun a bt)) b in
        let cl = List.combine (formal_parameters (List.length bflat)) (bflat) in
        Abs (nextvar, t, App (pfun c ct, App (Var nextvar, Choice cl)))
      | _ -> failwith "mauvais raffinement"
    end
  and nfun (a : ITC.inter_type) (t : ty) : terme = 
    let nextvar = next_variable () in 
    match a with
    | Base true -> TT
    | Base false -> FF
    | Map (b, c) -> begin
      match t with
      | Func(bt, ct) -> 
        let bflat = List.concat_map (fun (a, n) -> List.init n (fun _ -> pfun a bt)) b in
        Abs (nextvar, bt, If (App (and_iter bflat, Var nextvar), nfun c ct, omega))
      | _ -> failwith "mauvais raffinement"
    end
  in (pfun a t)

(*gènère, s'il existe, un contexte séparant pour m et n*)
let generate_contexte_separant (m:terme) (n:terme) : terme option =
  (*hypothèse : m et n sont sous forme normale*) 
  let (gm, _, tm) = jugement_of_terme m in
  let (gn, _, _) = jugement_of_terme n in
  let m_no = normal_of_terme m in
  let n_no = normal_of_terme n in
  let res = contextual_equivalence_aux m_no n_no (if (gm <> gn) then (delete_multiplicity (gn @ gm) (fun (v,_) (x,_) -> v=x)) else gm) tm
  in match res with
  | None -> None
  | Some (_, a) -> Some (contexte_separant a tm)
  (*Si gm et gn typent des variables différement, ou que tm <> tn, les types dictés par m priment *)
  

let rec binomial n k = 
  if (n<0 || k<0 || n<k) then 0 else
  if (k=0 && n = 0) then 1 else 
  (binomial (n-1) (k-1)) + (binomial (n-1) k) 

(*nombre de multiensemble de taille <= k prenant leurs éléments dans un ensemble de taille n*)
let mset_inf_k k n = binomial (k + n) k 

(* compte les alphas ◁a figurant des multisets de taille <= k *)
let it_size_k_count (k:int) (a:ty) : int =
  let rec aux (a:ty) : int =
    match a with
    | Base -> 2
    | Func(a,b) -> (aux b) * (mset_inf_k k (aux a))
  in aux a
let ic_size_k_count (k:int) (g:contexte) : int = List.fold_left (fun prod (_,t)-> prod * (mset_inf_k k (it_size_k_count k t))) 1 g


let tt:ITC.inter_type = Base true
let ff:ITC.inter_type = Base false
let test_pos_neg _ = 
  let print_pos a =
    print_string "Les éléments positifs de ";
    ITC.print_it a;
    print_string " sont : \n";
    List.iter (fun alpha -> print_it alpha ; print_string "\n") (pos a);
    print_string "---------------------\n";
  in
  let a:ITC.inter_type = ITC.canonical (Map ([(ff, 1) ; (tt, 3)], tt)) in
  let b:ITC.inter_type = ITC.canonical (Map ([(ff, 2) ; (tt, 2)], ff)) in
  let c:ITC.inter_type = ITC.canonical (Map ([(a, 1) ; (b, 1)], tt)) in
  let d:ITC.inter_type = ITC.canonical (Map ([(Map ([(ff, 1) ; (tt, 1)], ff), 2)], tt)) in
  let e:ITC.inter_type = ITC.canonical (Map ([(d, 2)], tt)) in
  print_pos (tt);
  print_pos (ff);
  print_pos (a);
  print_pos (b);
  print_pos (c);
(*   print_pos (d); *)
  print_pos (e);
  (* let (_,l) = seqlist_of_mult [(ff, 1) ; (tt, 3)] in
  List.iter (fun dec -> print_string "(  ";
    (List.iter (fun i -> print_int i ; print_string "; ") dec;
    print_string "\b\b)\n"
    )) l; *)
  let print_ic_neg g =
    print_string "Les éléments négatifs de ";
    ITC.print_ic g;
    print_string " sont : \n";
    List.iter (fun gg -> print_ic gg ; print_string "\n") (ic_neg g);
    print_string "---------------------\n";
  in
  print_ic_neg (ITC.ic_canonical [
    ('f', [(Map ([(tt,1)], tt),2)], Func(Base, Base)) ;
    ('x', [(tt,1)], Base) ;
    ] );
  ()

let test_eval _ =
  (* tests sur eval *)
  let general_test (m:Termes.terme) (g : ITC.inter_contexte) (a : ITC.inter_type) (expected_res : Rationals.t): unit =
      print_string"------------------------------------------------------------------------------------------------------\n";
      Termes.print_terme m ; print_string " : " ; ITC.print_it a ; print_char '\n';
      let m = (Termes.normal_of_terme m) in
      ITC.print_ic g ;
      let res = (eval m g a) in
      print_string "result = " ; Rationals.print res; print_char '\n';
      assert (res = expected_res);
  in

  general_test (Var 'x') (ITC.ic_canonical [
    ('x', [(tt,1)], Base) ;
    ] ) tt Rationals.one ;

  general_test (Var 'x') (ITC.ic_canonical [
    ('x', [(ff,1)], Base) ;
    ] ) tt Rationals.zero ;

  general_test (App (Var 'f', TT) ) (ITC.ic_canonical [
    ('f', [(Map ([(tt,1)], tt),1)], Func(Base, Base)) ;
    ] ) tt Rationals.one ;

  general_test (App (Var 'f', Var 'x') ) (ITC.ic_canonical [
    ('f', [(Map ([(tt,1)], tt),1)], Func(Base, Base)) ;
    ('x', [(tt,1)], Base) ;
    ] ) tt Rationals.one ;

  general_test (App (App (Var 'f', Var 'x'), Var 'y') ) (ITC.ic_canonical [
    ('f', [(Map ([(tt,1)], Map ([(tt,1)], tt)),1)], Func(Base, Func(Base, Base))) ;
    ('x', [(tt,1)], Base) ;
    ('y', [(tt,1)], Base) ;
    ] ) tt Rationals.one ;
  
  general_test (App (App (Var 'f', Var 'x'), Var 'x') ) (ITC.ic_canonical [
    ('f', [(Map ([(tt,1)], Map ([(tt,1)], tt)),1)], Func(Base, Func(Base, Base))) ;
    ('x', [(tt,2)], Base) ;
    ] ) tt Rationals.one ;
  
  general_test (App (Var 'f', App (Var 'f', Var 'x')) ) (ITC.ic_canonical [
    ('f', [(Map ([(tt,1)], tt),2)], Func(Base, Base)) ;
    ('x', [(tt,1)], Base) ;
    ] ) tt Rationals.one ;

  general_test (App (Var 'f', App (Var 'f', Var 'x')) ) (ITC.ic_canonical [
    ('f', [(Map ([(tt,1)], tt),1) ; (Map ([], tt),1)], Func(Base, Base)) ;
    ('x', [], Base) ;
    ] ) tt Rationals.one ;
    
  general_test (choice2 (Var 'x', Var 'y')) (ITC.ic_canonical [
    ('y', [(tt,1)], Base) ;
    ('x', [], Base) ;
    ] ) tt Rationals.one_half ;

  general_test (choice2 (Var 'x', Var 'y')) (ITC.ic_canonical [
    ('y', [(tt,1)], Base) ;
    ('x', [], Base) ;
    ] ) ff Rationals.zero ;

  general_test (choice2 (Var 'x', Var 'y')) (ITC.ic_canonical [
    ('y', [(tt,1)], Base) ;
    ('x', [(ff,1)], Base) ;
    ] ) ff Rationals.zero ;


  print_string"------------------------------------------------------------------------------------------------------\n";
  let m:Termes.terme = (App (App(Var 'f', App (App(Var 'f',Var 'y'), Var 'x')), App (App(Var 'f',Var 'x'), Var 'y'))) in
  Termes.print_terme m ; print_char '\n';
  let m = (Termes.normal_of_terme m) in

  let g = ITC.ic_canonical [
    ('f', [(Map([(tt, 1)], Map ([],tt)) , 1) ; (Map([], Map ([(tt,1)],tt)) , 1)], Func (Base, Func(Base, Base))) ;
    ('x', [(tt,1)], Base) ;
    ('y', [], Base) ;
    ] in
  ITC.print_ic g ;
  let res = (eval m g tt) in
  print_string "result = " ; Rationals.print res; print_char '\n';
  
  print_string"------------------------------------------------------------------------------------------------------\n";
  let m:Termes.terme = (App (Var  'f', App(Var 'g', Var 'y'))) in
  Termes.print_terme m ; print_char '\n';
  let m = (Termes.normal_of_terme m) in

  let g = ITC.ic_canonical [
    ('f', [(Map([(tt, 2)], tt) , 1)], Func (Base, Base)) ;
    ('g', [(Map([], tt),1) ; (Map([(tt,1)], tt), 1)], Func (Base, Base)) ;
    ('y', [(tt, 1)], Base) ;
    ] in
  ITC.print_ic g ;
  let res = (eval m g tt) in
  print_string "result = " ; Rationals.print res; print_char '\n';

  print_string"------------------------------------------------------------------------------------------------------\n";
  let m:Termes.terme = (App (Var  'f', Abs ('x', Base, App (Var 'g', Var 'x')))) in
  Termes.print_terme m ; print_char '\n';
  let m = (Termes.normal_of_terme m) in

  let g = ITC.ic_canonical [
    ('f', [(Map([(Map ([], tt), 1) ; (Map ([(tt,1)], tt), 1)], tt) , 1)], Func (Func (Base, Base), Base)) ;
    ('g', [(Map([], tt),1) ; (Map([(tt,1)], tt), 1)], Func (Base, Base)) ;
    ] in
  ITC.print_ic g ;
  let res = (eval m g tt) in
  print_string "result = " ; Rationals.print res; print_char '\n';

  print_string"------------------------------------------------------------------------------------------------------\n";
  let m:Termes.terme = (App (Var  'f', Abs ('x', Base, App (Var 'g', Var 'y')))) in
  Termes.print_terme m ; print_char '\n';
  let m = (Termes.normal_of_terme m) in

  let g = ITC.ic_canonical [
    ('f', [(Map([(Map ([], tt), 2)], tt) , 1)], Func (Func (Base, Base), Base)) ;
    ('g', [(Map([], tt),1) ; (Map([(tt,1)], tt), 1)], Func (Base, Base)) ;
    ('y', [(tt,1)], Base) ;
    ] in
  ITC.print_ic g ;
  let res = (eval m g tt) in
  print_string "result = " ; Rationals.print res; print_char '\n';

  print_string"------------------------------------------------------------------------------------------------------\n";

  let m:Termes.terme = (App (Var  'f', App (Var 'g', choice2 (TT, FF)))) in 
  Termes.print_terme m ; print_char '\n';
  let m = (Termes.normal_of_terme m) in

  let g = ITC.ic_canonical [
    ('f', [(Map([(tt,3)], tt), 1)], Func (Base, Base)) ;
    ('g', [(Map([], tt),1) ; (Map([(tt,1)], tt), 1) ; (Map([(tt,1) ; (ff,1)], tt), 1)], Func (Base, Base)) ;
    ] in
  ITC.print_ic g ;
  let res = (eval m g tt) in
  print_string "result = " ; Rationals.print res; print_char '\n';

  print_string"------------------------------------------------------------------------------------------------------\n";

  let m:Termes.terme = (App (Var  'f', App (Var 'g', choice2 (TT, FF)))) in 
  Termes.print_terme m ; print_char '\n';
  let m = (Termes.normal_of_terme m) in

  let g = ITC.ic_canonical [
    ('f', [(Map([(tt,3)], tt), 1)], Func (Base, Base)) ;
    ('g', [(Map([], tt),1) ; (Map([(tt,1)], tt), 2)], Func (Base, Base)) ;
    ] in
  ITC.print_ic g ;
  let res = (eval m g tt) in
  print_string "result = " ; Rationals.print res; print_char '\n';

  print_string"------------------------------------------------------------------------------------------------------\n"

let test_enum _ = 
  let is_canonical (l: ITC.inter_type list) : bool = 
    List.fold_left (fun accu t -> accu && (let res = (t = ITC.canonical t) in (if (not res) then ITC.print_it t); res)) true l
  in
  let is_decreasing (l: ITC.inter_type list) : bool =
    match l with
    | [] -> true
    | e::ll -> begin
      let rec aux (l: ITC.inter_type list) (last : ITC.inter_type) : bool =
        match l with
        | [] -> true
        | next::ll -> if ((ITC.compare last next) > 0) then (aux ll next) else (ITC.print_it last ; print_string "\n" ; ITC.print_it next; false)
        in aux ll e
    end
  in
  let test_generate_it a n =
    print_string "test de génération de cardinal d'au plus "; print_int n; print_string " pour le type "; print_t a; print_string " : ";
    let l = generate_it_list n a in
  (*   List.iter (fun t -> ITC.print_it t ; print_string "\n") l; *)
    assert (is_canonical l) ; 
(*     print_string "\n------------\n"; *)
    assert (is_decreasing l);
    print_int (List.length l) ; print_string " éléments.\n";
    assert ((List.length l) = (it_size_k_count n a));
  (*   print_string"------------------------------------------------------------------------------------------------------\n" *)
  in
  let a:ty = (Base) in
  test_generate_it a 0;
  test_generate_it a 1;
  let a:ty = (Func (Base, Func (Base, Base))) in
  test_generate_it a 0;
  test_generate_it a 1;
  test_generate_it a 2;
  test_generate_it a 3;
  test_generate_it a 4;
  let a:ty = (Func (Func (Func (Base, Base), Base), Func (Base, Func (Base, Base)))) in
  test_generate_it a 0;
  test_generate_it a 1;
  (* test_generate_it a 2; *)
  print_string"------------------------------------------------------------------------------------------------------\n";
  let test_generate_ic_list g k =
    print_string "test de génération de de contexte raffiné de taille "; print_int k; print_string " pour le contexte "; print_c g; print_string "\n";
    let res = generate_ic_list k g in
    let alt = delete_multiplicity res (=) in
    (* List.iter (fun t -> print_ic t ; print_string "\n") res; *)
    print_int (List.length res) ; print_string " contextes au total.\n";
    assert (alt = res);
    assert (List.length res = ic_size_k_count k g);
    print_string"------------------------------------------------------------------------------------------------------\n";
  in
  let g = [('x', Base) ; ('f', Func (Base, Base)) ; ('g', Func (Func (Base, Base), Base)) ; ('y', Base)] in
  test_generate_ic_list g 0;
  test_generate_ic_list g 1;
  ()

let test_equiv _ =
  let test_contextual_equivalence m n =
    print_terme m;
    print_string " et "; print_terme n;
    print_string " divergent sur : \n";
    match (contextual_equivalence m n) with
    | Some(g,a) -> ITC.print_ic g ; print_string "⊢ "; ITC.print_it a ;
    print_string " ("; Rationals.print (eval (normal_of_terme m) g a );
    print_string " <> "; Rationals.print (eval (normal_of_terme n) g a );
    print_string ")\n";
    print_string "Un contexte séparant m et n est : \n\n\t";
    begin match (generate_contexte_separant m n) with
    |Some c -> print_terme c; print_string "\n\n"
    |None -> failwith "erreur lors du test"
    end
    | None -> failwith "erreur lors du test"
  in
  test_contextual_equivalence TT FF;
  test_contextual_equivalence (Abs ('x',Base,TT)) (Abs ('x',Base,Var 'x')) ;
  test_contextual_equivalence (choice2 (TT, FF)) (choice2 (FF, FF));
  test_contextual_equivalence (Var 'x') (Var 'y');
  test_contextual_equivalence (Var 'x') (Abs ('y', Base, TT));
  test_contextual_equivalence (Abs ('y', Base, App (Var 'f', Abs ('x', Base, TT)))) (Abs ('z', Base, Abs ('y', Base, App (Var 'f', Abs ('x', Base, TT)))));
  test_contextual_equivalence (Var 'f') (Abs ('x', Base, App (Var 'f', Var 'x')));
  test_contextual_equivalence (Abs ('x', Base, App (Var 'f', Var 'x'))) (Var 'f');
  ()

let test_equiv_krivine _ =
  let iter = 5000 in
  let test_contextual_equivalence m n =
    print_terme m;
    print_string " et "; print_terme n;
    print_string " divergent sur : \n";
    match (contextual_equivalence m n) with
    | Some(g,a) -> ITC.print_ic g ; print_string "⊢ "; ITC.print_it a ;
    print_string " ("; Rationals.print (eval (normal_of_terme m) g a );
    print_string " <> "; Rationals.print (eval (normal_of_terme n) g a );
    print_string ")\n";
    print_string "Un contexte séparant m et n est : \n\n\t";
    let mc = cloture m in let nc = cloture n in
    begin match (generate_contexte_separant mc nc) with
    |Some c -> begin
      print_terme c; print_string "\n\n" ;
      print_string "Sur " ; print_int iter; print_string " itérations, les valeurs obtenues sont les suivantes : \n" ;
      let (mt, mf, mb) = Krivine.estimation_proba (App (c, mc)) iter in
      let (nt, nf, nb) = Krivine.estimation_proba (App (c, nc)) iter in
      print_string "\tm \tn \n";
      print_string "tt : \t" ; Rationals.print mt ; print_string "\t" ; Rationals.print nt ; print_string "\n";
      print_string "ff : \t" ; Rationals.print mf ; print_string "\t" ; Rationals.print nf ; print_string "\n";
      print_string "⊥  : \t" ; Rationals.print mb ; print_string "\t" ; Rationals.print nb ; print_string "\n";
      ()
      end
    |None -> failwith "erreur lors du test"
    end
    | None -> failwith "erreur lors du test"
  in
  test_contextual_equivalence TT FF;
  test_contextual_equivalence (Abs ('x',Base,TT)) (Abs ('x',Base,Var 'x')) ;
  test_contextual_equivalence (choice2 (TT, FF)) (choice2 (FF, FF));
  test_contextual_equivalence (Var 'x') (Abs ('y', Base, TT));
  test_contextual_equivalence (Abs ('x', Base, App (Var 'f', Var 'x'))) (Var 'f');
  test_contextual_equivalence (If(Var 'x', FF, TT)) (Var 'x');
  ()

let test_wrel _ =
  let separator = "==============================================================\n\n --------------------- test suivant ---------------------\n\n==============================================================\n" in
  (* test_inter_type ();
  print_string separator; *)
  Termes.test_termes ();
  print_string separator;
  test_pos_neg ();
  print_string separator;
  test_eval ();
  print_string separator;
  test_enum ();
  print_string separator;
  test_equiv ();
  print_string separator;
  Krivine.test_krivine ();
  print_string separator;
  test_equiv_krivine ();
  ()


open Inter_type_canonique

let tt = Base true
(* let ff = Base false *)

let x_i (i:int) : char = (char_of_int (i + (int_of_char 'A')))

let rec highorder (n:int) : (terme * inter_contexte * inter_type) =
  if (n <= 0) then (Var 'A', [('A', [(tt,1)], Base)] ,tt) else 
  let rec typ (n:int) : ty = 
    if (n<=0) then Base else let r = typ (n-1) in Func (r, r) in
  let (m, g, alpha) = highorder (n-1) in
  (Abs (x_i n, typ n, m), g, Map ([(alpha,1)], alpha))

let choice (n:int) : (terme * inter_contexte * inter_type) =
  let m1 = Choice (List.init n (fun _ -> (Rationals.create 1 n, Var (x_i 0)))) in
  let gamma:inter_contexte = [((x_i 0), [(Base true, n/2); (Base false, n-(n/2))], Base)] in
  let rec aux (n:int) : terme =
    if (n<=0) then m1
    else let m = aux (n-1) in
    Choice (List.init n (fun _ -> (Rationals.create 1 n, m)))
  in (aux n, gamma, Base true)

let ifterme (n:int) : (terme * inter_contexte * inter_type) =  
  let gamma:inter_contexte = [((x_i 0), [(Base true, n/2); (Base false, n-(n/2))], Base)] in
  let rec q (n:int) : terme = 
    if (n<= 0) then TT
    else If (Var (x_i 0), p (n-1), q (n-1))
  and p (n:int) : terme = 
    if (n<= 0) then FF
    else If (Var (x_i 0), q (n-1), p (n-1))
  in (p n, gamma, Base true)

let convert (f : int -> (terme * inter_contexte * inter_type)) : ( int -> (Termes.t * inter_contexte * inter_type)) =
  fun i -> let (a,b,c) = f i in (normal_of_terme a,b,c)

let rec time (iter:int) (f : int -> (Termes.t * inter_contexte * inter_type)) : (int * float) list =
  if (iter <= 0) then [] else
  let (m, ic, it) = f iter in
  let start = Unix.gettimeofday () in
  let _ = eval m ic it in
  let stop = Unix.gettimeofday () in
  (iter, stop-.start)::(time (iter-1) f)

let bench (id:int) (iter:int) = 
  let l = time iter ( convert
    (match id with
    | 0 -> highorder
    | 1 -> choice
    | _ -> ifterme)
  ) in
  let filename = "results/bench" ^ (string_of_int id) ^ ".csv" in
  let oc = open_out filename in
  Printf.fprintf oc "%s\n" "n\tt";
  (List.iter (fun (n, t)-> Printf.fprintf oc "%d\t%f\n" n t) l);
  close_out oc

let bench_all _ =
  for i = 2 to 2 do
    let _ = bench i 10 in ()
  done