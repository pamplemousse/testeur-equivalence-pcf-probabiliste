open Termes

type r = Rationals.t
type stack_elem = T of terme | PartialIf of (terme * terme)
type stack = stack_elem list
type state = (terme * stack)

val step_list : state -> (r * state) list

val random_step : state -> state

val is_stop_state : state -> bool

val random_execute : state -> state

val estimation_proba : terme -> int -> r * r * r

val test_krivine : unit -> unit