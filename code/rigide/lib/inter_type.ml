type inter_type =
  | Base of bool
  | Map of (inter_type_mset * inter_type)

and inter_type_mset = (inter_type) list

let card (a : inter_type_mset) : int = List.length a

(* (* false < true *)
let bool_compare (a : bool) (b : bool) : int =
  match (a, b) with
  | (true, false) -> 1
  | (false, true) -> -1
  | _ -> 0

(* hypothèse : les entrées de compare et ms_compare sont sous forme canonique *)
let rec compare (a : inter_type) (b : inter_type) : int =
  match (a, b) with
  | (Base b1, Base b2) -> bool_compare b1 b2
  | (Base _, _) -> -1
  | (Map (ma, sa), Map (mb, sb)) -> let cmp = compare sa sb in
      if (cmp != 0) then cmp else (ms_compare ma mb) (* évite des appels récursifs/comparaisons inutiles*)
  | (Map _, Base _) -> 1

(* ordre lexicographique *)
and ms_compare (a : inter_type_mset) (b : inter_type_mset) : int = 
  let na = card a in let nb = card b in
  if (na <> nb) then (na - nb)
  else 
    let rec aux (a : inter_type_mset) (b : inter_type_mset) : int =
      match (a, b) with
      | ([], []) -> 0
      | (_, []) -> 1
      | ([], _) -> -1
      | ((t1, n1)::aa, (t2,n2)::bb) -> let cmp = compare t1 t2 in
      if (cmp <> 0) then cmp else (* évite des appels récursifs/comparaisons inutiles*)
      if (n1 <> n2) then (n2 - n1) else (aux aa bb)
    in aux a b
and canonical (a : inter_type) : inter_type =
  match a with
  | Base _ -> a
  | Map (m, b) -> Map (ms_canonical m, canonical b)

and ms_canonical (m : inter_type_mset) : inter_type_mset = 
  let step1 = (List.map (fun (a,n) -> (canonical a, n)) m) in
  let step2 = List.sort (fun (a,_) (b,_) -> compare b a) step1 in (* order is reversed*)
  fuse step2 (* reversed again*)

and fuse (m : inter_type_mset) : inter_type_mset =
  let rec aux (m : inter_type_mset) (last : (inter_type * int) option) (res : inter_type_mset) : inter_type_mset =
    match (m, last) with
    | ([], None) -> res
    | ([], Some(v,i)) -> (v,i)::res
    | ((e,n)::mm, None) -> aux mm (Some (e,n)) res
    | ((e,n)::mm, Some(v,i)) ->
        if (compare e v = 0) then (aux mm (Some (e, n+i)) res)
        else (aux mm (Some (e,n)) ((v,i)::res))
  in aux m None []

let create = canonical
let create_ms = ms_canonical

let add_elem (a: inter_type) (n : int) (m : inter_type_mset) : inter_type_mset =
  (*hyp : a et m sont sous forme canonique*)
  let rec aux (m : inter_type_mset) : inter_type_mset =
    match m with
    | [] -> [(a,n)]
    | (b,k)::mm -> 
      let cmp = compare a b in
      if (cmp = 0) then ((b,k+n)::mm) else (*a = b*)
      if (cmp < 0) then ((a,n)::(b,k)::mm) (*a < b*)
      else (b,k)::(aux mm) (*a > b*)
  in aux m
 *)

let eqtype (a:inter_type) (b:inter_type) = (a=b)
let eqtypemset (ma:inter_type_mset) mb = (ma = mb)

let rec print_it (a : inter_type) =
  match a with
  | Base b -> if b then print_string "tt" else print_string "ff" 
  | Map (m, s) -> print_mset m ; print_string " ⊸ " ; print_it s
and print_mset (m : inter_type_mset) = print_char '[' ; (List.iter (fun a -> (print_it a ; print_string " ; "))  m); 
(if (m <> []) then print_string "\b\b\b") ; print_char ']'


open Termes
(* type ty = Termes.ty (* | Base | Func of (ty * ty) *)
type variable = char *)

let rec print_t (a : ty) = match a with Base -> print_char 'B'
  | Func (b, c) -> print_char '(' ; print_t b ; print_string "→" ; print_t c ; print_char ')'
  
type inter_contexte = (variable * inter_type_mset * ty) list 

type r = Rationals.t

let print_ic (g : inter_contexte) = List.iter (fun (v,m,t)-> 
  print_char v ; print_string " : " ; print_mset m ; print_string " ◁ " ; print_t t ; print_char '\n') g

let ic_canonical g =
  let cmp = (fun (va,_,_) (vb,_,_) -> Char.compare va vb) in List.sort cmp g

(* égalité de contextes raffinés *)
let eq_ic (g : inter_contexte) (d : inter_contexte) : bool = ( g = d)
  (* let eq = fun (v1, i1, t1) (v2, i2, t2) -> (v1 = v2) && (t1 = t2) && (eqtypemset i1 i2) in
  eqmultiset g d eq *)

(* let rec generate_ic l:((variable * inter_type_mset) list) : inter_contexte =
  match l with [] -> []
  | (v,m)::ll -> () *)

(* égalité entre décomposition (= liste/tupple de contextes) *)
let eq_dec (l1 : inter_contexte list) (l2 : inter_contexte list) : bool = 
  try List.for_all2 (eq_ic) l1 l2 with _ -> false (*cas des longueurs distinctes*)

(* si n éléments de l sont égaux, supprime les n-1 premières occurences *)
let rec delete_multiplicity (l : 'a list)  (eq : 'a -> 'a -> bool) : 'a list =
  match l with [] -> []
  | e::ll -> let r = (delete_multiplicity ll eq) in
  if (List.exists (eq e) r) then r else (e::r)

  (* liste les paires de séquences distinctes se concaténant pour forme la séquence en entrée *)
let list_of_multisets_pairs (l : 'a list) : ('a list * 'a list) list =
  let rec aux (l : 'a list) (past : 'a list) : ('a list * 'a list) list =
    (List.rev past, l)::
    match l with 
    | [] -> []
    | e :: ll -> (aux ll (e::past))
  in aux l []

(* liste les paires de sous-contextes distinctes formant le contexte donné *)
let rec list_of_subcontext_pairs (g : inter_contexte) : (inter_contexte * inter_contexte) list = 
  match g with [] -> []
  | (v, m, ty) :: g -> 
      let partial_contexts = (list_of_subcontext_pairs g) in
      let multiset_pairs = (list_of_multisets_pairs m) in
      (* on ajoute chaque paire de multiset possible à chaques paires de "contexte partiel"*)
      let f = (fun (l,r) ->
        List.map (fun (ml, mr) -> (((v, ml, ty)::l), ((v, mr, ty)::r))) multiset_pairs
      ) in List.concat_map f (if (partial_contexts <> []) then partial_contexts else [([], [])])

(* liste les décompositions distinctes d'un contexte donné *)
let rec list_of_contexte_decomposition (n:int) (g:inter_contexte) : (inter_contexte list) list = 
  assert(n>0); if (n=1) then [[g]] else 
  List.concat_map (fun (l,r) -> List.map (fun e -> l :: e) (list_of_contexte_decomposition (n-1) r) ) (list_of_subcontext_pairs g)
  (* fixe le premier élément avec list_of_subcontext_pairs, et s'appelle récursivement pour définir les n-1 éléments restants *)

(* concatenation de deux contextes*)
(* let conc_ic (g:inter_contexte) (h:inter_contexte) : inter_contexte =
  List.map2 (fun (v,m1,t) (_,m2,_) -> (v,m1 @ m2,t)) g h *)

let rec is_null (g:inter_contexte) : bool =
  match g with [] -> true
  | (_, [], _) :: gg -> is_null gg
  |  _ -> false


let test_inter_type (_:unit) : unit = 
  ()