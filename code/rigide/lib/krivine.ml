open Termes

type r = Rationals.t
type stack_elem = T of terme | PartialIf of (terme * terme)
type stack = stack_elem list
type state = (terme * stack)

let print_state (t, s) : unit =
  print_string "(" ; print_terme t; print_string ", [";
  (if (s = []) then print_string "]" else
    (List.iter (fun v ->
      print_string "(";
      match v with
      | T t -> print_terme t
      | PartialIf (n,p) -> print_string "if _ then "; print_terme n ; print_string " else " ; print_terme p
      ;
      print_string "), "
      ) s ; print_string "\b\b]"));
  print_string ")"

let rec substitution (u : terme) (x: variable) (v : terme) : terme = (* u[x:=v] *)
  match u with
  | Abs (z, t, w) -> if (x=z) then u (*alpha renommage*)
    else Abs (z, t, (substitution w x v)) 
  | Var y -> if (y=x) then v else u
  | App (l, r) -> App (substitution l x v, substitution r x v)
  | If (m, n, p) -> If (substitution m x v, substitution n x v, substitution p x v)
  | Choice l -> Choice (List.map (fun (r,m) -> (r, substitution m x v)) l)
  | _ -> u (* TT, FF, Bot *)

let step_list ((m,pi):state) : (r * state) list =
  let one = Rationals.one in
  match (m, pi) with
  | (App(u, v), _) -> [(one, (u, (T v)::pi))]
  | (Abs(x, _, u), (T v)::pi) -> [(one, (substitution u x v, pi))]
  | (Choice l, _) -> List.map (fun (q, m) -> (q, (m, pi))) l
  | (If(m,n,p), _) ->  [(one, (m, (PartialIf (n,p))::pi))]
  | (TT, PartialIf(n,_)::pi) ->  [(one, (n, pi))]
  | (FF, PartialIf(_,p)::pi) ->  [(one, (p, pi))]
  | _ -> [] (*configurations stoppantes*)

let random_step ((m,pi):state) : state =
  let smax = 536870912 in (*2^29*)
  let v = Random.int (smax) in
  let threshold = Rationals.create v smax in
  let rec aux (l : (r * 'a) list) (t : r) : 'a =
    match l with
    | [] -> (Bot, [])
    | (q,a)::ll -> let new_t = Rationals.add t q in
      if (Rationals.ge new_t threshold) then a
      else (aux ll new_t)
  in (aux (step_list (m,pi)) Rationals.zero)

let is_stop_state ((m,pi):state) : bool = (*configurations stoppantes atteignables*)
  match (m, pi) with
  | (Var _, _) 
  | (TT, []) 
  | (FF, []) 
  | (Abs _, []) 
  | (Bot, _) -> true
  | _ -> false

let rec random_execute (s:state) : state = 
  if (is_stop_state s) then s else begin
    assert ( not (is_stop_state s));
    (random_execute (random_step s)) end

(* estime la probabilité que c termine sur tt, ff ou ne termine pas*)
let estimation_proba (c : terme) (iter : int) : (r * r * r) =
  let rec aux (i : int) (rt : int)  (rf : int)  (rb : int): int * int * int =
    if (i <= 0) then (rt, rf, rb) else
      match (random_execute (c, [])) with
      | (TT, _) ->  aux (i-1) (rt+1) (rf) (rb)
      | (FF, _) ->  aux (i-1) (rt) (rf+1) (rb)
      | _ ->        aux (i-1) (rt) (rf) (rb+1)
  in let (a, b, c) = (aux iter 0 0 0) in
  (Rationals.create a iter, Rationals.create b iter, Rationals.create c iter)

let test_krivine _ = 
  let test_estimation m =
    let _ = print_state in
    let (a, b, c) = estimation_proba m 100000 in
    print_string "Les probabilités de résultats de " ; print_terme m ; print_string " est : \n" ;
    print_string "tt : " ; Rationals.print a ; print_string "\n";
    print_string "ff : " ; Rationals.print b ; print_string "\n";
    print_string "⊥  : " ; Rationals.print c ; print_string "\n";

  in
  test_estimation TT;
  test_estimation FF;
  test_estimation (Choice [(Rationals.one_half, TT);(Rationals.one_half, FF)]);
  test_estimation (Choice [(Rationals.create 1 10, TT);(Rationals.create 9 10, FF)]);
  let m = (Choice [(Rationals.create 1 10, TT);(Rationals.create 4 10, FF)]) in
  test_estimation m;
  test_estimation (If(m, FF, TT));
  test_estimation (App (Abs('x', Base, Var 'x'), m));
  ()